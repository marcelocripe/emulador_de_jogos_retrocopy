pt-BR:

O emulador de jogos RetroCopy foi criado por RetroRalph. Emula os jogos do Nintendo, Master System, Game Gear, SG-1000, Mega Drive, Genesis e de Fliperama ou Arcade da Sega System E.

Contém: o arquivo ou pacote de instalação .deb e um arquivo de texto em idioma "pt-BR" explicando como utilizá-lo.

Transfira ou baixe ou descarregue os arquivos ".zip" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Der Spieleemulator RetroCopy wurde von RetroRalph erstellt. Emuliert Nintendo, Master System, Game Gear, SG-1000, Mega Drive, Genesis und Arcade Sega System E.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR"-Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien ".zip" und die Textdatei ".txt" herunter oder laden Sie sie herunter oder laden Sie sie herunter.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

fr :

L'émulateur de jeu RetroCopy a été créé par RetroRalph. Émule les jeux Nintendo, Master System, Game Gear, SG-1000, Mega Drive ou alors Genesis et Arcade Sega System E.

Contient : le fichier ou package d'installation .deb et un fichier texte en "pt-BR" expliquant comment l'utiliser.

Téléchargez les fichiers ".zip" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

L'emulatore di gioco RetroCopy è stato creato da RetroRalph. Emula i giochi Nintendo, Master System, Game Gear, SG-1000, Mega Drive, Genesis e Arcade Sega System E.

Contiene: il file o pacchetto di installazione .deb e un file di testo in lingua "pt-BR" che spiega come utilizzarlo.

Scarica i file ".zip" e il file di testo ".txt".

Tutti i crediti e i diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe

- - - - -

es:

El emulador de juegos RetroCopy fue creado por RetroRalph. Emula juegos Nintendo, Master System, Game Gear, SG-1000, Mega Drive, Genesis et Arcade Sega System E.

Contiene: el archivo o paquete de instalación .deb y un archivo de texto en idioma "pt-BR" que explica cómo usarlo.

Descargue los archivos ".zip" y el archivo de texto ".txt".

Todos los créditos y derechos están incluidos en los archivos, en relación con el trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

en:

The RetroCopy game emulator was created by RetroRalph. Emulates Nintendo, Master System, Game Gear, SG-1000, Mega Drive, Genesis and Arcade Sega System E games.

Contains: the .deb installation file or package and a text file in "pt-BR" language explaining how to use it.

Download the files ".zip" and the text file ".txt".

All credits and rights are included in the files, in respect of the volunteer work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe
